import React from 'react';
import Moment from 'react-moment';
import 'moment-timezone';

const Message = props => {
	return (
		<li>
			<span className="autor">{props.author}</span>
			<span className="date"><Moment format='DD MMM YYYY, HH:mm' date={props.datetime}/></span>
			<p className="text">{props.message}</p>
		</li>
	)
};

export default Message;