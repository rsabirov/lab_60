import React, {Component} from 'react';
import './Chat.css';
import axios from 'axios';
import SendBlock from "./SendBlock/SendBlock";
import Messages from "./Messages/Messages";

class Chat extends Component {
	
	state = {
		messages: [],
		author: 'Alex_Egor_Rus',
		messageText: '',
		lastMessageTime: 0
	};
	
	makeTextMessage = (event) => {
		this.setState({messageText: event.target.value});
	};
	
	getMessages = async () => {
		try {
			let messages = await axios.get(`http://146.185.154.90:8000/messages?datetime=${this.state.lastMessageTime}`);
			this.setState({messages: messages.data});
		} catch (error) {
			console.log(error);
		}
	};
	
	sendMessage = async () => {
		const data = new URLSearchParams();
		data.append('author', this.state.author);
		data.append('message', this.state.messageText);
		axios.post('http://146.185.154.90:8000/messages', data);
		this.setState({messageText: ''});
		clearInterval(this.interval);
		await this.getMessages();
		this.startInterval();
	};
	
	changeAuthor = (event) => {
		this.setState({author: event.target.value});
	};
	
	componentDidMount() {
		this.getMessages();
		this.startInterval();
	}
	
	interval = null;
	
	startInterval = () => {
		this.interval = setInterval(() => {
			this.getMessages();
		}, 3000);
	};
	
	render() {
		return (
			<div className="container">
				<Messages
					messages={this.state.messages}
				/>
				<SendBlock
					author={this.state.author}
					makeTextMessage={(event) => this.makeTextMessage(event)}
					sendMessage={() => this.sendMessage()}
					changeAuthor={(event) => this.changeAuthor(event)}
					text={this.state.messageText}
				/>
			</div>
		)
	}
}

export default Chat;